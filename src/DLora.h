#ifndef DLORA_H
#define DLORA_H

#include <LoRa.h>

using ReceiveCallback = void (*)(int);

class DLora
{
public:
    static void InitLoRa(ReceiveCallback Callback);

    static void SendMessage(const String& InMessage);
};

#endif
