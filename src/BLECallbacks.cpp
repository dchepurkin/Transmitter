#include "BLECallbacks.h"
#include "DLora.h"
#include "DPackage.h"

void ConnectCallback::onDisconnect(BLEServer* pServer)
{
    if(pServer)
    {
        pServer->startAdvertising();
    }
}
void ConnectCallback::onConnect(BLEServer* pServer)
{
    DLora::SendMessage(GET_STATE);
}

void StartEngineCallback::onWrite(BLECharacteristic* pCharacteristic)
{
    const auto IsEnabled = *pCharacteristic->getData() == 0x01;
    DLora::SendMessage(IsEnabled ? START_ENGINE : STOP_ENGINE);
}
