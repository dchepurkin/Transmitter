#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include "BLECallbacks.h"
#include "DLora.h"
#include "DPackage.h"

#define SERVICE_UUID "C6FBDD3C-7123-4C9E-86AB-005F1A7EDA01"
#define CHARACTERISTIC_UUID_RX "B88E098B-E464-4B54-B827-79EB2B150A9F"
#define CHARACTERISTIC_UUID_TX "D769FACF-A4DA-47BA-9253-65359EE480FB"

BLECharacteristic* Characteristic;

void OnReceive(int PackageSize)
{
    if(PackageSize)
    {
        String InMessage("", PackageSize);

        if(LoRa.available())
        {
            LoRa.readBytes(InMessage.begin(), PackageSize);
        }

        Serial.println(InMessage);
    }
}

void setup()
{
    Serial.begin(9600);
    DLora::InitLoRa(OnReceive);

    BLEDevice::init("Chip Auto Run");

    const auto Server = BLEDevice::createServer();
    Server->setCallbacks(new ConnectCallback());

    const auto Service = Server->createService(SERVICE_UUID);

    Characteristic = Service->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_WRITE);
    Characteristic->setCallbacks(new StartEngineCallback());

    Service->start();
    Server->getAdvertising()->setScanFilter(true, false); // �������� ������ (������ ���������� ����������, ������������ ����� ������ �� ��� ������)
    Server->startAdvertising();
}

void loop() { delay(10); }