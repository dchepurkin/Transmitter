#ifndef BLECALLBACKS_H
#define BLECALLBACKS_H

#include <Arduino.h>
#include <BLEServer.h>

class ConnectCallback : public BLEServerCallbacks
{
    void onConnect(BLEServer* pServer) override;

    void onDisconnect(BLEServer* pServer) override;
};

class StartEngineCallback : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic* pCharacteristic) override;
};

#endif