#ifndef DPACKAGE_H
#define DPACKAGE_H

#include "WString.h"

#define START_ENGINE String("START")
#define STOP_ENGINE String("STOP")

#define GET_STATE String("GETSTATE")

#define ACC_STATE String("ACC")
#define ON_STATE String("ON")
#define WORK_STATE String("WORK")
#define START_ERROR String("STARTERROR")

#define ERROR String("ERROR")

#endif